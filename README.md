# skuSample

#### DEMO
[http://yukisola.gitee.io/skusample](http://yukisola.gitee.io/skusample)

#### 介绍
1. 根据sku自动生成商品选择交互
1. HTML5 API + 原生JS
1. 使用了大量循环和重绘,性能不佳,然能达到目的


#### 使用说明

``` 
    var sku = new Sku({...})
    sku._init()
```

